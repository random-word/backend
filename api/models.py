from django.db import models


class Word(models.Model):
    CORRECT = "C"
    WRONG = "W"
    BLANK = "B"

    status_choices = [
        (CORRECT, "Correct"),
        (WRONG, "Wrong"),
        (BLANK, "Blank")
    ]

    word = models.CharField(max_length=50, unique=True)
    pronunciation = models.CharField(max_length=50, blank=True)
    description = models.TextField()
    translation = models.CharField(max_length=50, blank=True)
    example = models.TextField(blank=True)
    example_translation = models.TextField(blank=True)
    status = models.CharField(max_length=2, choices=status_choices, default=BLANK)

    def __str__(self) -> str:
        return self.word
