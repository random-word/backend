from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework import status
from .serializers import WordSerializer
from .utils import get_random_word
from .models import Word


class WordListAPI(APIView):

    def post(self, request: Request) -> Response:
        serializer = WordSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request: Request) -> Response:
        words = Word.objects.all()
        serializer = WordSerializer(words, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
def random_word(request: Request) -> Response:
    word = get_random_word()
    if word:
        serializer = WordSerializer(word)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response("No word has been found", status=status.HTTP_404_NOT_FOUND)


class WordAPI(APIView):

    def get(self, request: Request, pk: int) -> Response:
        word = Word.objects.filter(id=pk).first()
        if word:
            serializer = WordSerializer(word)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response("Word not found", status=status.HTTP_404_NOT_FOUND)

    def put(self, request: Request, pk: int) -> Response:
        word = Word.objects.filter(id=pk).first()
        if not word:
            return Response("Word not found with given pk", status=status.HTTP_404_NOT_FOUND)
        serializer = WordSerializer(word, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
