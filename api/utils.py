from .models import Word
import random
from typing import Union


def get_random_word() -> Union[Word, None]:
    ids = Word.objects.values_list("id")
    if ids:
        rand_id: tuple = random.choice(ids)
        rand_word = Word.objects.get(id=rand_id[0])
        return rand_word
    return None
