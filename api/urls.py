from django.urls import path
from . import views

urlpatterns = [
    path("words/", views.WordListAPI.as_view(), name="word-list"),
    path("random-word/", views.random_word, name="random-word"),
    path("word/<int:pk>/", views.WordAPI.as_view(), name="word")
]
