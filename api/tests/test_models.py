from django.test import TestCase
from api.models import Word
from api.serializers import WordSerializer


class TestModels(TestCase):

    def test_word_required_fields_succeeds(self):
        word = {
            "word": "apple",
            "translation": "alma",
            "description": "vegetable"
        }

        serializer = WordSerializer(data=word)

        self.assertEquals(serializer.is_valid(), True)

    def test_word_required_fields_fails(self):
        word = {
            "word": "apple",
            # "description": "vegetable"
        }

        serializer = WordSerializer(data=word)

        self.assertEquals(serializer.is_valid(), False)
