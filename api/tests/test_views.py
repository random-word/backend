from django.test import TestCase, Client
from django.urls import reverse
from api.models import Word


class TestWordListView(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.client = Client()
        cls.url = reverse("word-list")

    @classmethod
    def tearDownClass(cls) -> None:
        pass

    def test_word_list_GET_succeeds(self) -> None:
        response = self.client.get(self.url)
        self.assertEquals(response.status_code, 200)

    def test_word_list_POST_succeeds(self) -> None:
        word = {
            "word": "apple",
            "translation": "alma",
            "pronunciation": "/eppl/",
            "description": "vegetable",
            "example": "I eat apple",
            "example_translation": "Mən alma yeyirəm",
            "status": "B"
        }

        response = self.client.post(self.url, word)
        word["id"] = 1

        self.assertEquals(response.status_code, 201)  # Object has been created
        self.assertEquals(response.json(), word)  # Created object data is matching with sent body

    def test_word_list_POST_fails(self) -> None:
        word = {
            "word": "apple",
            "translation": "alma",
            # "description": "vegetable" # <- required field
        }

        response = self.client.post(self.url, word)
        self.assertEquals(response.status_code, 400)


class TestWordView(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.client = Client()
        cls.url = reverse("word", args=[1])
        Word.objects.create(
            word="apple",
            translation="alma",
            pronunciation="/eppl/",
            description="vegetable",
            example="I eat apple",
            example_translation="Mən alma yeyirəm",
            status="B"
        )

    @classmethod
    def tearDownClass(cls) -> None:
        pass

    def test_word_GET_succeeds(self) -> None:
        response = self.client.get(self.url)
        self.assertEquals(response.status_code, 200)

    def test_word_GET_fails(self) -> None:
        url = reverse("word", args=[2])  # no word with given id

        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)

    def test_word_PUT_succeeds(self) -> None:
        response = self.client.put(self.url, {
            "status": "C"
        }, content_type="application/json")
        self.assertEquals(response.status_code, 200)

    def test_word_PUT_fails(self) -> None:
        url = reverse("word", args=[2])  # no word with given id

        response = self.client.put(url, {
            "status": "C"
        }, content_type="application/json")
        self.assertEquals(response.status_code, 404)


class TestRandomView(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.client = Client()
        cls.url = reverse("random-word")

    @classmethod
    def tearDownClass(cls) -> None:
        pass

    def test_random_word_GET_succeeds(self) -> None:
        Word.objects.create(
            word="apple",
            translation="alma",
            description="vegetable"
        )
        response = self.client.get(self.url)
        self.assertEquals(response.status_code, 200)

        response_json = response.json()
        self.assertEquals(response_json["word"], "apple")

    def test_random_word_GET_fails(self) -> None:
        response = self.client.get(self.url)
        self.assertEquals(response.status_code, 404)
