from django.test import TestCase
from api.views import WordListAPI, WordAPI, random_word
from django.urls import reverse, resolve


class TestUrls(TestCase):

    def test_word_list_api_resolves(self) -> None:
        url = reverse("word-list")
        self.assertEqual(resolve(url).func.view_class, WordListAPI)

    def test_word_api_resolves(self) -> None:
        url = reverse("word", args=[1])
        self.assertEqual(resolve(url).func.view_class, WordAPI)

    def test_random_word_api_resolves(self) -> None:
        url = reverse("random-word")
        self.assertEqual(resolve(url).func, random_word)
