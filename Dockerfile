FROM python:3.8.7

WORKDIR /app
COPY requirements.txt /app
RUN pip install -r requirements.txt
COPY . .

EXPOSE 8000
ENTRYPOINT ["/bin/bash", "start.sh"]
