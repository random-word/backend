# shellcheck disable=SC2013

# ********* FOR JSON ENV FILES *********
#for keyval in $(grep -E '": [^\{]' env.json | sed -e 's/: /=/' -e "s/\(\,\)$//"); do
#    echo "export $keyval"
#    eval export "$keyval"
#done

echo "Migrating database"
python manage.py migrate
echo "Collecting static files"
python manage.py collectstatic --noinput
echo "Starting server"
python manage.py runserver 0.0.0.0:8000 --noreload