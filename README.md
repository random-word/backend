# RandomWord - backend
This project is REST API backend service written in Django REST framework.
Whole purpose of this app was to generate(get) random words from database
to rehearse english words while studying for exams.

## Requirements
For running up this project you only need [docker-compose](https://docs.docker.com/compose/install/) to be installed.

You need two(for dev and prod) files for environment variables named exactly as **.env.dev**
and **.env.prod**. You can check which variables need for each in files named as
env.dev and env.prod. 

## Usage
After everything set up, you can simply write:
```bash
docker-compose -f docker-compose.prod.yaml up 
```
And the app will run on 0.0.0.0:8000.

## API

### Get list of words

#### Request
```GET /words/```

```bash
    curl -i -H "Accept: application/json" http://localhost:8000/words/
```
#### Response
```bash
    []
```

### Create a word
#### Required fields are: 
"word", "description"<br>
#### All fields: 
"word", "description", "pronunciation", "translation", "example", "example_translation", "status"

#### Request
```POST /words/```

```bash
    curl -i  -H "Content-type: application/json" -d {"word":"apple", "description": "fruit"} -x POST http://localhost:8000/words/
```
#### Response
```bash
    {   
        "id":1, 
        "word": "apple",
        "description": "fruit", 
        "translation": "",
        "prononciation": "", 
        "example": "", 
        "example_translation": "",
        "status": "B"
    }
```
### Get a word

#### Request
```GET /word/<int: id>```

```bash
    curl -i  -H "Content-type: application/json" http://localhost:8000/word/1
```
#### Response
```bash
    {   
        "id":1, 
        "word": "apple",
        "description": "fruit", 
        "translation": "",
        "prononciation": "", 
        "example": "", 
        "example_translation": "",
        "status": "B"
    }
```
### Update a word
```PUT /word/<int: id>```

```bash
    curl -i  -H "Content-type: application/json" -d {"status": "C"} -x POST http://localhost:8000/word/1/
```
#### Response
```bash
    {   
        "id":1, 
        "word": "apple",
        "description": "fruit", 
        "translation": "",
        "prononcuation": "", 
        "example": "", 
        "example_translation": "",
        "status": "C"
    }
```